import { LocationProvider } from './../../providers/location/location';
import { Component } from '@angular/core';
import { NavController, Platform, IonicPage } from 'ionic-angular';
import { GoogleMaps, GoogleMap, GoogleMapsEvent, LatLng, Circle } from '@ionic-native/google-maps';

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  map: any;
  userCurrentLocationMarker: any;

  constructor(
    public navCtrl: NavController,
    public platform: Platform,
    private location: LocationProvider
  ) {
    platform.ready().then(() => {
      console.log("platform.ready");
    });
  }

  ionViewDidEnter() {
    this.loadMap();
  }

  loadMap() {
    console.log('loadMap');
    this.location.createMap('map').then(map => {
      this.map = map;
      this.location.getLocation().then((locationData: any) => {
        this.createCurrentLocationMarker(locationData);
        this.addCircle(locationData)
      });
    });
  }

  createCurrentLocationMarker(location) {
    console.log("createCurrentLocationMarker", location);
    if (!this.userCurrentLocationMarker) {
      this.userCurrentLocationMarker = this.map.addMarker({
        position: location
      });
    } else {
      this.userCurrentLocationMarker.setPosition(location);
    }
    this.map.animateCamera({
      target: location
    });
  }

  addCircle(location){
    this.map.addCircle({
      'center': location,
      'radius': this.map.getCameraZoom() * 10,
      'strokeColor': '#AA00FF',
      'strokeWidth': 5,
      'fillColor': '#00880055'
    });
  }


}

import { Component } from '@angular/core';
import { IonicPage, ViewController, Platform } from 'ionic-angular';
import { Diagnostic } from '@ionic-native/diagnostic';
import { LocationAccuracy } from '@ionic-native/location-accuracy';
/**
 * Generated class for the PermissionPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-permission',
  templateUrl: 'permission.html',
})
export class PermissionPage {

  constructor(
    public locationAccuracy: LocationAccuracy,
    public diagnostic: Diagnostic,
    public platform: Platform,
    public viewCtrl: ViewController
  ) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PermissionPage');
  }

  allowRequest() {
    this.locationAccuracy.canRequest().then((canRequest: boolean) => {
      if (this.platform.is('ios')) {
        this.diagnostic.switchToSettings();
      } else {
        this.diagnostic.switchToLocationSettings();
      }
      let onAppResume = () => {
        document.removeEventListener('resume', onAppResume);
        this.diagnostic.isLocationEnabled().then(
          (isAvailable) => {
            if (isAvailable) {
              this.viewCtrl.dismiss(true);
            }
          }).catch((e) => {
            console.log("Setting error:", e);
          });
      }
      document.addEventListener('resume', onAppResume);
    });
  }

  denyRequest() {
    this.viewCtrl.dismiss();
  }

}

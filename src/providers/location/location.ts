import { GoogleMap } from '@ionic-native/google-maps';
import { GLOBALCONFIG, MAPOPTIONS } from './../config/config';
import { Geolocation } from '@ionic-native/geolocation';
import { Diagnostic } from '@ionic-native/diagnostic';
import { Injectable } from '@angular/core';
import { ModalController, Platform } from 'ionic-angular';
/*
  Generated class for the LocationProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class LocationProvider {
    public currentlocationIcon;

    constructor(
        private geolocation: Geolocation,
        private diagnostic: Diagnostic,
        private modalCtrl: ModalController,
        private platform: Platform
    ) {
        console.log('Hello LocationProvider Provider');
    }

    userLastLocation() {
        return new Promise((resolve) => {
            let ls = localStorage.getItem('LASTLOCATION');
            if (ls == null) {
                localStorage.setItem("LASTLOCATION", JSON.stringify(GLOBALCONFIG.location));
                resolve(GLOBALCONFIG.location);
            } else {
                resolve(JSON.parse(ls));
            }
        });
    }

    createMap(mapElement) {
        return new Promise((resolve, reject) => {
            let ls = localStorage.getItem('LASTLOCATION');
            if (ls == null) {
                MAPOPTIONS.camera.target = GLOBALCONFIG.location;
            } else {
                MAPOPTIONS.camera.target = JSON.parse(ls);
            }
            let map = new GoogleMap(mapElement, MAPOPTIONS);
            resolve(map);
        });
    }

    getLocation() {
        console.log("Location Service");
        return new Promise((resolve, reject) => {
            this.diagnostic.isLocationEnabled().then(
                (isAvailable) => {
                    console.log('location enable diagnostic', isAvailable);
                    if (isAvailable) {
                        console.log("Location Service => LocationAuthorized call");
                        this.LocationAuthorized().then(location => {
                            resolve(location);
                        });
                    } else {
                        console.log("Location Service => LocationCheck call");
                        this.LocationCheck("location").then(location => {
                            resolve(location);
                        });
                    }
                }).catch((e) => {
                    this.LocationCheck("location").then(location => {
                        resolve(location);
                    });
                });
        });
    }

    //2
    LocationCheck(mode) {
        return new Promise((resovle) => {
            let locationCheckModal = this.modalCtrl.create('PermissionPage', { modelType: 'locationCheck', modeType: mode });
            locationCheckModal.present();
            locationCheckModal.onDidDismiss(isAllowed => {
                console.log("Location Service => LocationCheck :", isAllowed);
                if (isAllowed) {
                    console.log("Location Service => LocationCheck => LocationAuthorized call");
                    this.LocationAuthorized().then(location => {
                        resovle(location);
                    });
                } else {
                    console.log("Location Service => LocationCheck => default");
                    this.userLastLocation().then((location: any) => {
                        resovle(location);
                    });
                }
            })
        });
    }

    //1
    LocationAuthorized() {
        return new Promise((resovle) => {
            this.diagnostic.getLocationAuthorizationStatus().then(status => {
                console.log("getLocationAuthorizationStatus", status);
                if (status == 'denied') {
                    console.log('Location Service => LocationCheck => LocationAuthorized => requestLocationAuthorization || getlocation call');
                    this.LocationCheck("locationAuth").then(location => {
                        resovle(location);
                    });
                } else {
                    console.log('Location Service => LocationCheck => LocationAuthorized => getlocation call');
                    this.getCurrentLocation().then(location => {
                        resovle(location);
                    });
                }
            });
        });
    }

    getCurrentLocation() {
      console.log("getCurrentLocation");
        return new Promise((resovle) => {
            let options = { timeout: 5000, enableHighAccuracy: true };
            this.geolocation.getCurrentPosition(options).then((resp) => {
              console.log("getCurrentLocation",resp);
              let location = { lat: resp.coords.latitude, lng: resp.coords.longitude };
                localStorage.setItem("LASTLOCATION", JSON.stringify(location));
                resovle(location);
            }).catch((error) => {
              console.log("getCurrentLocation error",error);
                this.userLastLocation().then((location: any) => {
                    resovle(location);
                });
            });
        });
    }
}
